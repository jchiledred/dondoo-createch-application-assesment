﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Human {


    protected string firstName;
    protected string surname;
    protected int age;

//a simple function for the father to speak
    public abstract void Speak ();

 //calculates the living cost
    public abstract float CalculateLivingCost (float cost);

}