﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Father : Human {
    //declaring the Father constructor
    public Father (string newName, string newSurname, int newAge) {
        firstName = newName;
        surname = newSurname;
        age = newAge;
    }

    //Overrideing the human function

    public override void Speak () {
        Debug.Log ($"hello my name is{firstName} {surname} and i am a father");

    }

    //Overrideing the human function
    public override float CalculateLivingCost (float cost) {
        return cost * age;
    }
}