﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Family : MonoBehaviour {

// list of all the humans
    List<Human> family = new List<Human> ();

    // creates three humans and then runs the code for each of them.
    void Start () {
        family.Add (new Father ("John", "Smith", 30));
        family.Add (new Son ("Josh", "Smith", 15));
        family.Add (new Son ("Jedd", "Smith", 10));

        foreach (Human human in family) {
            human.Speak ();
            Debug.Log ($"i cost this much {human.CalculateLivingCost(100)}");
        }
    }
}