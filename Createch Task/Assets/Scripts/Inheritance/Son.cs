﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Son : Father {

//son constructor
    public Son (string newName, string newSurname, int newAge) : base (newName, newSurname, newAge) {
        firstName = newName;
        surname = newSurname;
        age = newAge;

    }

// overide of the Human function being inherited from the father
    public override void Speak () {
        Debug.Log ($"hello my name is{firstName} {surname} and i am a son");

    }

// overide of the Human function being inherited from the father
    public override float CalculateLivingCost (float cost) {
        return base.CalculateLivingCost (cost) + 100;
    }

}