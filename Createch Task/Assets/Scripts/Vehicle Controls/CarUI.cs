﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarUI : MonoBehaviour {
    //acceleration image
    [SerializeField] Image acl;
    //brake image;
    [SerializeField] Image brk;
    // reverse image;
    [SerializeField] Image rev;

    //4 wheel drive image;
    [SerializeField] Image fwd;
    void Update () {

        if (VehicleManager.Instance.isAccelerating) {
            Debug.Log ("is acl");
            acl.color = new Color32 (255, 0, 0, 100);
        } else {
            acl.color = new Color32 (255, 255, 255, 100);
        }

        if (VehicleManager.Instance.isBrakeing) {
            brk.color = new Color32 (255, 0, 0, 100);
        } else {
            brk.color = new Color32 (255, 255, 255, 100);
        }
        if (VehicleManager.Instance.isReversing) {
            rev.color = new Color32 (255, 0, 0, 100);
        } else {
            rev.color = new Color32 (255, 255, 255, 100);
        }
        if (VehicleManager.Instance.fourWheelDrive) {
            fwd.color = new Color32 (255, 0, 0, 100);
        } else {
            fwd.color = new Color32 (255, 255, 255, 100);
        }

    }
}