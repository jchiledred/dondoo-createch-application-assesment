﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VehicleManager : MonoBehaviour {

    //setting up the singleton
    private static VehicleManager _instance;
    public static VehicleManager Instance {
        get {
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<VehicleManager> ();
            }
            return _instance;
        }
    }
    void Awake () {
        DontDestroyOnLoad (gameObject);
        _instance = this;
    }

    //When Setting Up the Vehicle Make sure that you have the correct 
    // hierachy for the wheels. This Should be as Follows:
    /*
        CarRoot
            carbody
            Wheels----- these will be the models of the wheels
                Front Left
                Front Right
                Back Left
                Back Right
            Wheel Colliders----- These will be the individual wheel colliders and 
                                 and Should no have any models
                Front Left
                Front Right
                Back Left
                Back Right
    */

    // float to store Horizontal input. Used to control left/right movement
    float m_horizontalInput;
    // float to store Vertical input. Used to control forward/back movement
    float m_verticalInput;
    //Float to store your steering angle
    float m_steerAngle;

    [Header ("Assignable Car Variables")]

    //Stores the Wheel Colider Components of The Vehicle;
    [Tooltip ("Please assign the Wheel Colliders To this Variable")]
    public WheelCollider frontLeftW;
    public WheelCollider frontRightW;
    public WheelCollider backLeftW;
    public WheelCollider backRightW;

    //Stores the Transform of  the Wheel for the Vehicle;
    [Tooltip ("Please assign the Wheel objects To this Variable")]
    public Transform frontLeftT, frontRightT, backLeftT, backRightT;

    [Space (5)]
    [Header ("Change the car steering and 2x4/4x4 capabilities")]
    //bool to enable  4 wheel steering
    [Tooltip ("Use this to enable four wheel steering")]
    public bool fourWheelSteering = false;
    //bool to enable 4 wheel drive

    [Tooltip ("Use this to enable  four wheel drive")]
    public bool fourWheelDrive = false;

    //sets the maximum Steering Angle;\
    [Tooltip ("Maximum Steering Angle")]
    public float maxSteerAngle = 30;

    //the value by which your forward movement is accelerated
    [Tooltip ("Value that determins your acceleration")]
    [Range (50, 500)]
    public float accelerationFactor = 50;

    //  the value used for your breaking

    [Tooltip ("value used to determin your brake force")]
    [Range (50, 500)]
    public float brakeingFactor = 200;

    //the following three bools are all for access in the CarUI manager to determine which visuals it needs to access

    [HideInInspector]
    public bool isBrakeing = false;

    [HideInInspector]
    public bool isAccelerating = false;

    [HideInInspector]
    public bool isReversing = false;

    // gets the input values based on stanard WASD or Arrow keys
    public void GetInput () {
        m_horizontalInput = Input.GetAxis ("Horizontal");
        m_verticalInput = Input.GetAxis ("Vertical");

    }

    //Calculates and sets the wheels Steer angle for turning and steering
    public void Steer () {
        m_steerAngle = maxSteerAngle * m_horizontalInput;
        frontRightW.steerAngle = m_steerAngle;
        frontLeftW.steerAngle = m_steerAngle;
        if (fourWheelSteering) {
            backRightW.steerAngle = -m_steerAngle;
            backLeftW.steerAngle = -m_steerAngle;
        }

    }

    // Applies the acceleration factor to the cars forward and backwards movement 
    void Accelerate () {

        frontLeftW.motorTorque = m_verticalInput * accelerationFactor;
        frontRightW.motorTorque = m_verticalInput * accelerationFactor;
        if (fourWheelDrive) {
            backLeftW.motorTorque = m_verticalInput * accelerationFactor;
            backRightW.motorTorque = m_verticalInput * accelerationFactor;
        }
        if (m_verticalInput > 0) {
            //Debug.Log($"is accle: {isAccelerating}");
            isAccelerating = true;
            isReversing = false;
        } else if (m_verticalInput < 0) {
            isReversing = true;
            isAccelerating = false;

        } else if (m_verticalInput == 0) {
            isReversing = false;
            isAccelerating = false;

        }
        Debug.Log ($"is accle: {isAccelerating}");

    }

    void Break () {
        isBrakeing = false;
        if (Input.GetKey (KeyCode.Space)) {

            isBrakeing = true;
            frontLeftW.brakeTorque = brakeingFactor;
            frontRightW.brakeTorque = brakeingFactor;

            backLeftW.brakeTorque = brakeingFactor;
            backRightW.brakeTorque = brakeingFactor;

        }
        if (Input.GetKeyUp (KeyCode.Space)) {
            frontLeftW.brakeTorque = 0;
            frontRightW.brakeTorque = 0;

            backLeftW.brakeTorque = 0;
            backRightW.brakeTorque = 0;
        }
    }

    //Updates all of the wheel models Positions for when the car is suspended 
    //or a wheel is hanging
    void UpdateWheelPoses () {
        UpdateWheelPose (frontLeftW, frontLeftT);
        UpdateWheelPose (frontRightW, frontRightT);
        UpdateWheelPose (backLeftW, backLeftT);
        UpdateWheelPose (backRightW, backRightT);

    }

    void UpdateWheelPose (WheelCollider _collider, Transform _transform) {
        Vector3 _pos = _transform.position;
        Quaternion _quat = _transform.rotation;

        _collider.GetWorldPose (out _pos, out _quat);
        _transform.position = _pos;
        _transform.rotation = _quat;
    }
    void FixedUpdate () {
        GetInput ();
        Steer ();
        Accelerate ();
        UpdateWheelPoses ();

    }
    void Update () {
        Break ();

    }

}